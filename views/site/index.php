<?php

/* @var $this yii\web\View */
/* la cartella web è quella che viene presa da yii*/

$this->title = 'My Yii Application';
?>
<div class="site-index">
<!--  invece di fare  <style>
    .carousel-cell {width: 100%}
</style>
 in web css site.css
 .jumbotron elemento di bootstrap contenitore già stilizzato il cui contenuto 
 sia visualizzato in un certo modo
 
-->
	<div class="carousel">
	
	<!--  data-flickity='{ "cellAlign": "left", 
	"contain": true, "groupCells":false, "autoplay": 1500 }'>
	
	Ora questo lo inizializziamo noi attraverso il js 
	
	-->
  <div class="carousel-cell" data-flickity-bg-lazyload="/images/slide01.jpg">
      	<div class="centrato">
          	<h3> Titolo del primo articolo</h3>
          	<a class="button read-more" href="#">Scopri</a>
      	</div>
  	</div>
 <div class="carousel-cell" data-flickity-bg-lazyload="/images/slide02.jpg">
  		<div class="centrato">
          	<h3>Titolo del secondo articolo</h3>
          	<a class="button read-more" href="#">Scopri</a>
      	</div>
	</div>
  <div class="carousel-cell" data-flickity-bg-lazyload="/images/slide03.jpg">
        <div class="centrato">
                <h3>Titolo del terzo articolo</h3>
                <a class="button read-more" href="#">Scopri</a>
         </div>
  	</div>
  <div class="carousel-cell" data-flickity-bg-lazyload="/images/slide04.jpg">
      <div class="centrato">
          <h3>Titolo del quarto articolo</h3>
          <a class="button read-more" href="#">Scopri</a>
      </div>
  	</div>
</div>
</div>

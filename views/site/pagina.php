<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Prima pagina Yii2 generata a mano</h2>

   <ol>
   	<li>Creare nella cartella delle view un file php con il nome che voglio dare </li>
   	<li>Creare nel controller desiderato una nuova funzione action</li>
   	<li>RIchiamare nella funzione action attraverso il render la view desiderator</li>
   	
   
   </ol>

    <code><?= __FILE__ ?></code>
</div>
